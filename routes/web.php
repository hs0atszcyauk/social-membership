<?php

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware' => ['auth', 'role:Admin']], function () {
    Route::get('/', 'Panel\DashboardController@index')->name('dash');
    Route::resource('user', 'Panel\UserController');
    Route::post('approve/{id}', 'Panel\UserController@approve');
    Route::post('ban/{id}', 'Panel\UserController@ban');
    Route::post('unban/{id}', 'Panel\UserController@unban');

    Route::resource('reward', 'Panel\RewardController');
    Route::get('/reward/{id}/add', 'Panel\RewardController@add');
    Route::get('/reward/{id}/{pointId}/edit', 'Panel\RewardController@edit');
    Route::delete('/reward/{id}/{pointId}/delete', 'Panel\RewardController@destroy');
    Route::put('/reward/{id}/{pointId}', 'Panel\RewardController@update');

    Route::resource('event', 'Panel\EventController');
    Route::get('/event/{eventId}/{userId}/add', 'Panel\EventController@add');
    Route::get('/event/{eventId}/{userId}/withdraw', 'Panel\EventController@withdraw');

    Route::resource('feedback', 'Panel\FeedbackController');
    Route::get('feedback/{user}/list', 'Panel\FeedbackController@user_list');
    Route::put('feedback/{feedback}/chat', 'Panel\FeedbackController@update_chat');

    Route::resource('notification', 'Panel\NotificationController');
    Route::get('notification-devices', 'Panel\NotificationController@devices');

    Route::resource('receipts', 'Panel\ReceiptController');
    Route::post('receipts-approve/{id}', 'Panel\ReceiptController@approve');

    Route::resource('shop', 'Panel\ShopController');


    Route::group(['middleware' => ['role:Admin']], function () {
        /**
         *  Setting
         */
        Route::get('settings', 'Panel\SettingsController@index');
        Route::get('settings/git-pull', 'Panel\SettingsController@git_pull');
    });
});

Route::get('/', function () {
    if (Auth::check()) {
        if (Auth::user()->hasRole('Admin')) {
            return redirect()->route(ADMIN . '.dash');
        } else {
            Auth::logout();
            return view('auth.login')->withErrors(['email' => 'You do not have permission to login']);
        }
    } else {
        return view('auth.login');
    }
});

Route::fallback(function () {
    return response()->view('errors.404', [], 404);
});
