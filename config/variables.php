<?php

return [

    'boolean' => [
        '0' => 'No',
        '1' => 'Yes',
    ],

    'status' => [
        '1' => 'Active',
        '0' => 'Inactive',
    ],

    'avatar' => [
        'public' => '/storage/avatar/',
        'folder' => 'avatar',

        'width'  => 32,
        'height' => 32,
    ],

    'tables_name' => [
        '001' => 'setting_currency',
        '002' => 'setting_event_type',
        '003' => '_users',
        '004' => '_password_resets',
        '006' => 'notifications',
        '007' => 'notification_devices',
        '008' => 'events',
        '009' => 'event_users',
        '010' => 'receipts',
        '011' => 'feedbacks',
        '012' => 'user_point_records',
        '013' => 'setting_feedback_type',
        '014' => 'shop_item',
        '015' => 'notification_users'
    ],

    'feedback_type' => [
        0 =>  'Good',
        1 =>  'Bad',
        2 =>  'Suggestion',
    ],

    'user_status' => [
        0 => 'Approving',
        1 => 'Approved',
        2 => 'Banned'
    ],

    'receipt_status' => [
        0 => 'Waiting For Approving',
        1 => 'Waiting For Revising',
        2 => 'Disapproved',
        3 => 'Approved',
    ],

    'image' => [
        'receipt' => env('APP_URL', 'http://localhost') . '/storage/image/receipt/',
        'events' => env('APP_URL', 'http://localhost') . '/storage/image/events/',
        'notifications' => env('APP_URL', 'http://localhost') . '/storage/image/notifications/',
    ],

    'folder' => [
        'receipt' =>  'image/receipt/',
        'feedback' =>  'image/feedback/',
        'events' => 'image/events/',
        'notifications' => 'image/notifications/',
    ],

    'location' => [
        1 =>  'Hong Kong',
        2 =>  'China',
        3 =>  'USA',
        4 =>  'Japan',
    ],

    'currency' => [
        1 =>  'HKD',
        2 =>  'RMB',
        3 =>  'USD',
        4 =>  'EUR',
        5 => 'GBP',
    ],

    'expense_type' => [
        1 =>  'Transport',
        2 =>  'Meal',
        3 =>  'Purchase',
        4 => 'Other',
    ],

    'notification_type' => [
        1 => 'News',
        2 => 'Others'
    ],

    'event_type' => [
        1 => 'Activity',
        2 => 'Lecture',
    ],

    'payment_type' => [
        1 =>  'Cash',
        2 =>  'Cheque',
        3 =>  'Credit Card',
        4 => 'Other',
    ],
    /*
    |------------------------------------------------------------------------------------
    | ENV of APP
    |------------------------------------------------------------------------------------
    */
    'APP_ADMIN' => 'admin',

];
