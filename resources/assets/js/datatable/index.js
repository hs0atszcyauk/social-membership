import * as $ from 'jquery';
import 'datatables';

export default (function () {
  $('#dataTable').DataTable();
  $('#dataTable2').DataTable();
  $('#dataTable3').DataTable();
  $('#dataTable-reward').DataTable({
    "order": [[0, "desc"]],
  });
}())
