@extends('admin.default')

@section('page-header')
User <small>{{ trans('app.detail_item') }}</small>
@stop


@section('content')

@include('admin.panel.user.partial.panel_detail')

{!! Form::model($user) !!}
@include('admin.panel.user.partial.form')
{!! Form::close() !!}
@stop