{{-- Panel --}}
@if($mode['isModeShow'])
<div class="row mB-10">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">
      <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
      {{-- @if(auth()->user()->can('user-edit') || auth()->user()->can('user-profile'))
      <a href="{{ URL::action('Panel\UserController@edit',$user->id) }}" class="btn cur-p btn-primary">Edit</a>
      @endif
      @if($user['id']!=auth()->user()->id)
      @can('user-delete')
      {!! Form::open([
      'class'=>'delete d-inline',
      'url' => URL::action('Panel\UserController@destroy', $user['id']),
      'method' => 'DELETE', ])
      !!}
      <button class="btn btn-danger" title="{{ trans('app.delete_title') }}">Delete</button>
      {!! Form::close() !!}
      @endcan
      @endif --}}
      @if($user->status === 0)
      {!! Form::open([
      'class'=>' d-inline',
      'url' => URL::action('Panel\UserController@approve', $user->id),
      'method' => 'POST', ])
      !!}
      <button class="btn btn-warning" title="Approve User">Approve</button>
      {!! Form::close() !!}
      @elseif($user->status === 1)
      {!! Form::open([
      'class'=>'delete d-inline',
      'url' => URL::action('Panel\UserController@ban', $user->id),
      'method' => 'POST', ])
      !!}
      <button class="btn btn-danger" title="Ban User">Ban</button>
      {!! Form::close() !!}
      @else
      {!! Form::open([
      'class'=>' d-inline',
      'url' => URL::action('Panel\UserController@unban', $user->id),
      'method' => 'POST', ])
      !!}
      <button class="btn btn-info" title="Unban User">Unban</button>
      {!! Form::close() !!}
      @endif


      <a href="{{ URL::action('Panel\RewardController@show', $user->id) }}" class="btn btn-primary float-right ml-1">
        <i class="ti-star"></i>
        Reward record
      </a>
      <a href="{{ URL::action('Panel\EventController@index', $user->id) }}"
        class="btn btn-primary float-right ml-1">Event record</a>
    </div>
  </div>
</div>
@endif