<div class="row mB-40">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">
      <div class="form-row">
        <div class="col d-flex justify-content-center align-items-center">
          @if((!$mode['isModeCreate']))
          <img src="{{ $user->avatar }}" alt="" class="w-100p bdrs-50p m-auto " style="max-width: 150px">
          @else
          <img src="{{ asset('images/avatar.png') }}" alt="" class="w-100p bdrs-50p m-auto " style="max-width: 150px">
          @endif
        </div>
        <div class="col-10">
          <div class="form-row">
            <div class="col">
              {!! Form::myInput('text', 'name[first]', 'First Name',
              ($mode['isModeShow'])?['readonly','required']:['required']) !!}
            </div>
            <div class="col">
              {!! Form::myInput('text', 'name[last]', 'Last Name',
              ($mode['isModeShow'])?['readonly','required']:['required']) !!}
            </div>
          </div>
          <div class="form-row">
            <div class="col">
              {!! Form::myInput('email', 'email', 'Email',
              ($mode['isModeShow'])?['readonly','required']:['required']) !!}
              <!-- {!! Form::myInput('email', 'email', 'Email', ['class'=>'form-control form-control-plaintext text-success
              p-0','readonly','required']) !!} -->
            </div>
            <div class="col">
              {!! Form::myInput('phone', 'phone', 'Phone', ($mode['isModeShow'])?['readonly']:[]) !!}
            </div>
          </div>
        </div>
      </div>

      {!! Form::myInput('text', 'address', 'Address', ($mode['isModeShow'])?['readonly']:[]) !!}

      <div class="form-group">
        {!! Form::label('birth', 'Date of birth') !!}
        <div class="input-group date" @if((!$mode['isModeShow'])) data-provide="datepicker" data-date-end-date="0d"
          data-date-format="yyyy/mm/dd" @endif>
          <input type="text" name="birth" class="form-control"
            value="@if(($mode['isModeEdit']) || ($mode['isModeShow'])) {{auth()->user()->birth}} @endif"
            @if(($mode['isModeShow'])) readonly @endif required>
          <div class="input-group-addon">
            <span class="ti-calendar"></span>
          </div>
        </div>
      </div>

      @if($mode['isModeCreate'])
      {!! Form::myInput('password', 'password', 'Password', ['required']) !!}
      {!! Form::myInput('password', 'password_confirmation', 'Password again', ['required']) !!}
      @endif


      <div class="form-group row">
        <div class="col">
          @if(!$mode['isModeShow'])
          {!! Form::label('role', 'Role') !!}
          <div class="form-row">
            @foreach($roles as $id => $name)
            <div class="col-3">
              @php $selected=false; if(!$mode['isModeCreate'])if($user->hasRole($name))$selected = true; @endphp
              {!! Form::myCheckbox('role['.$id.']',$name, $id, $selected) !!}
            </div>
            @endforeach
          </div>
          @else
          <h6>Role :
            @php $roleCount = 0; @endphp
            @foreach($roles as $role)
            @if($user->hasRole($role->name))
            <span class='mt-1 mb-1 rounded bg-primary p-4 text-light d-inline-block'>{{$role->name}}</span>
            @php $roleCount++; @endphp
            @endif
            @endforeach
            @if($roleCount==0)
            <span class='mt-1 mb-1  rounded bg-primary p-4 text-light d-inline-block'>Null</span>
            @endif
          </h6>
          @endif
        </div>
        @if($mode['isModeShow'])
        <div class="col-3">
          <h6>Point :
            <span class='m-2 rounded bg-warning p-4 text-dark'>{{$user->point}}</span>
          </h6>
        </div>
        @endif
      </div>

      <div class="form-group">
        {!! Form::label('currency', 'Currency') !!}
        <select name="currency" class="form-control" @if($mode['isModeShow'])disabled="disabled" @endif>
          @foreach($currencies as $currency)
          @if(!$mode['isModeCreate'])
          @if($user['currency']==$currency['id'] || $currency->status==1)
          <option value="{{ $currency['id'] }}" @if($mode['isModeShow'] || $mode['isModeEdit'])
            @if($user['currency']==$currency['id'])selected="selected" @endif @endif>{{ $currency['currency']}}</option>
          @endif
          @else
          <option value="{{ $currency['id'] }}" @if($mode['isModeShow'] || $mode['isModeEdit'])
            @if($user['currency']==$currency['id'])selected="selected" @endif @endif>{{ $currency['currency']}}</option>
          @endif
          @endforeach
        </select>
      </div>

      @if(!$mode['isModeShow'])
      {!! Form::myFile('avatar', 'New Avatar' ,['accept'=>'image/*']) !!}
      @endif

      {!! Form::myTextArea('remarks', 'Remarks', ($mode['isModeShow'])?['readonly']:[]) !!}
    </div>
  </div>
</div>