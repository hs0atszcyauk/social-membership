@extends('admin.default')

@section('page-header')
Event - <small>{{ $events->title.' - '.trans('app.add_new_item') }}</small>
@stop

@section('content')
{!! Form::model($events,[
'action' => ['Panel\EventController@update',$events->id],
'files' => true,
'method' => 'put', ])
!!}

@include('admin.panel.event.partial.form')

<div class="text-center">
    <button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
    <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
</div>

{!! Form::close() !!}


@stop