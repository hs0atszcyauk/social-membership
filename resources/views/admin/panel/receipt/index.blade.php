@extends('admin.default')

@section('page-header')
Receipt <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="bgc-white bd bdrs-3 p-20 mB-20">

      <ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="approving-tab" data-toggle="tab" href="#approving" role="tab"
            aria-controls="approving" aria-selected="true">Approving</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved"
            aria-selected="false">Approved</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="disapprove-tab" data-toggle="tab" href="#disapprove" role="tab"
            aria-controls="disapprove" aria-selected="false">Disapprove</a>
        </li>
      </ul>

      <div class="tab-content" id="tabContent">
        <div class="tab-pane fade show active" id="approving" role="tabpanel" aria-labelledby="approving-tab">
          <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Title</th>
                <th>User</th>
                <th>Location</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>

              @foreach ($approving as $row)
              <tr>
                <td>
                  <a href="{{ URL::action('Panel\ReceiptController@show', $row['id']) }}">
                    {{ $row->title }}
                  </a>
                </td>

                <td>{{ $model->find($row->user_id)->email }}</td>

                <td>{{ config('variables.location.' . $row->location) }}</td>

                <td>{{ $row->amount }}</td>

                <td>
                  <ul class="list-inline">

                    <li class="list-inline-item">
                      <a href="{{ URL::action('Panel\ReceiptController@show', $row['id']) }}" title="show"
                        class="btn btn-primary btn-sm">
                        <span class="ti-view-list-alt"></span>
                      </a>
                    </li>
                  </ul>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
          <table id="dataTable2" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Title</th>
                <th>User</th>
                <th>Location</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>

              @foreach ($approved as $row)
              <tr>
                <td>
                  <a href="{{ URL::action('Panel\ReceiptController@show', $row['id']) }}">
                    {{ $row->title }}
                  </a>
                </td>

                <td>{{ $model->find($row->user_id)->email }}</td>

                <td>{{ config('variables.location.' . $row->location) }}</td>

                <td>{{ $row->amount }}</td>

                <td>
                  <ul class="list-inline">

                    <li class="list-inline-item">
                      <a href="{{ URL::action('Panel\ReceiptController@show', $row['id']) }}" title="show"
                        class="btn btn-primary btn-sm">
                        <span class="ti-view-list-alt"></span>
                      </a>
                    </li>
                  </ul>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="disapprove" role="tabpanel" aria-labelledby="disapprove-tab">
          <table id="dataTable3" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Title</th>
                <th>User</th>
                <th>Location</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>

              @foreach ($disapprove as $row)
              <tr>
                <td>
                  <a href="{{ URL::action('Panel\ReceiptController@show', $row['id']) }}">
                    {{ $row->title }}
                  </a>
                </td>

                <td>{{ $model->find($row->user_id)->email }}</td>

                <td>{{ config('variables.location.' . $row->location) }}</td>

                <td>{{ $row->amount }}</td>

                <td>
                  <ul class="list-inline">

                    <li class="list-inline-item">
                      <a href="{{ URL::action('Panel\ReceiptController@show', $row['id']) }}" title="show"
                        class="btn btn-primary btn-sm">
                        <span class="ti-view-list-alt"></span>
                      </a>
                    </li>
                  </ul>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>

@endsection