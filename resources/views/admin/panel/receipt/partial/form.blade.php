<div class="row mB-40">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">

      <div>
        {!! Form::myInput('text', 'title', 'Title',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'transacted_at', 'Transacted at',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'currency_id', 'Currency',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'location', 'Location',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'expense_type', 'Expense type',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'payment_type', 'Payment type',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'amount', 'Amount',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

      <div>
        {!! Form::myInput('text', 'remarks', 'Remark',
        ($mode['isModeShow'])?['disabled','required']:['required']) !!}
      </div>

    </div>
  </div>
</div>