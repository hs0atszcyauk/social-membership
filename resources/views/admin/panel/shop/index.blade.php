@extends('admin.default')

@section('page-header')
Shop <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

<div class="mB-20 text-right">
    <button type="button" style="width:100px" class="btn btn-primary" data-toggle="modal"
        data-target="#exampleModalCenter">
        Add
    </button>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            {!! Form::open([
            'action' => ['Panel\ShopController@store'],
            'files' => false
            ])
            !!}
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Create New Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col">
                        {!! Form::myInput('text', 'title', 'Title'.' <span style="color:red">*</span>',array('required'
                        =>
                        'required')) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        {!! Form::myTextArea('description', 'Description'.' <span
                            style="color:red">*</span>',array('required' =>
                        'required')) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        {!! Form::myInput('number', 'points', 'Points'.' <span
                            style="color:red">*</span>',array('required' =>
                        'required')) !!}
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Points</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->points}}</td>
                            <td>
                                {!! Form::open([
                                'class'=>'delete',
                                'url' => URL::action('Panel\ShopController@destroy', $item->id),
                                'method' => 'DELETE', ])
                                !!}
                                <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}">
                                    <i class="ti-trash"></i>
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection