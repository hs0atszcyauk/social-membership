<div class="pb-2">
    <div class="m-auto">
        <a href="{{ URL::action('Panel\FeedbackController@index') }}" class="btn cur-p btn-secondary">Back</a>

        @if($mode['isModeShow'])
            @can('feedback-edit')
                <a href="{{ URL::action('Panel\FeedbackController@edit',$feedback->id) }}" class="btn cur-p btn-primary">Edit</a>
            @endcan
        @else
            <button type="submit" class="btn cur-p btn-primary">@if(!$mode['isModeEdit']) Save @else Update @endif</button>
        @endif

    </div>
</div>