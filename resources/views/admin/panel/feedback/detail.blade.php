@extends('admin.default')

@section('page-header')
@if($mode['isModeShow']) Feedback <small>No. {{$feedback->id}}</small> @endif
@if($mode['isModeEdit']) Feedback <small>No. {{$feedback->id}} Edit @endif
    @if($mode['isModeCreate']) Feedback Create @endif
    @endsection


    @section('content')

    <div class="col-lg-10 m-auto">
        <form class="user" method="post"
            action="@if(!$mode['isModeEdit']){{URL::action('Panel\FeedbackController@store')}}@elseif($mode['isModeEdit']){{URL::action('Panel\FeedbackController@update', $feedback->id)}}@endif"
            enctype="multipart/form-data">
            {{ csrf_field() }}
            @if($mode['isModeEdit'])
            {{method_field('put')}}
            @endif
            <div class="bgc-white p-20 bd mb-3">
                @include('admin.panel.feedback.partial.panel_detail')
            </div>
            @if($mode['isModeEdit'])
            <div class="bgc-white p-20 bd">
                <div class="row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="status">Status</label>
                        <select name="status" class="form-control form-control-lg">
                            @foreach(config('constants.FEEDBACK_STATUS') as $status => $value)
                            <option value="{{$value}}" @if($value==$feedback->status) selected @endif>{{$status}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="priority">Priority</label>
                        <select name="priority" class="form-control form-control-lg">
                            @foreach(config('constants.FEEDBACK_PRIORITY') as $priority => $value)
                            <option value="{{$value}}" @if($value==$feedback->priority) selected @endif>{{$priority}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" name="feedback_id" value="{{$feedback->id}}">
            @endif
            <div class="bgc-white p-20 bd">
                @if(!$mode['isModeCreate'])
                <div class="form-group">
                    <label for="status">Status</label>
                    @switch($feedback->status)
                    @case(config("constants.FEEDBACK_STATUS.OPEN"))
                    <span class='badge badge-success'>Open</span>
                    @break
                    @case(config("constants.FEEDBACK_STATUS.COMPLETED"))
                    <span class='badge badge-warning'>Completed</span>
                    @break
                    @case(config("constants.FEEDBACK_STATUS.CLOSED"))
                    <span class='badge badge-secondary'>Closed</span>
                    @break
                    @case(config("constants.FEEDBACK_STATUS.CANCELLED"))
                    <span class='badge badge-danger'>Cancelled</span>
                    @break
                    @default
                    <span class='badge badge-dark text-light'>Unknown</span>
                    @endswitch
                </div>
                @endif
                <div class="form-group">
                    <label for="content[title]">Title</label>
                    <input type="text" value="@if(!$mode['isModeCreate']){{$feedback->content['title']}}@endif"
                        class="form-control @if(!$mode['isModeCreate']) form-control-lg form-control-plaintext @endif"
                        id="content[title]" name="content[title]" required>
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type"
                        class="form-control @if(!$mode['isModeCreate']) form-control-plaintext disabled form-control-lg @endif"
                        @if(!$mode['isModeCreate']) disabled @endif>
                        @foreach($types as $type)
                        <option value="{{$type->id}}" @if(!$mode['isModeCreate'] && $type->id == $feedback->type)
                            selected @endif>{{$type->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="content[body]">Content</label>
                    <textarea
                        class="form-control @if(!$mode['isModeCreate']) form-control-plaintext form-control-lg @endif"
                        name="content[body]" rows="5"
                        required>@if(!$mode['isModeCreate']){{$feedback->content['body']}}@endif</textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label class="form-control-label">Image</label>
                    @if($mode['isModeCreate'])
                    <input type="file" class="form-control" name="file_upload[]" multiple accept="image/*">
                    <small id="fileHelp" class="form-text text-muted">Please upload a valid image file formats (e.g.
                        jpg, png).</small>
                    <small id="fileHelp" class="form-text text-muted">Size of the image should not be more than
                        2MB.</small>
                    <small id="fileHelp" class="form-text text-muted">Can choose multiple files.</small>
                    @else
                    @if(sizeof($feedback->files) > 0)
                    <style>
                        @media (min-width: 576px) {
                            .card-columns {
                                column-count: 2;
                            }
                        }

                        @media (min-width: 992px) {
                            .card-columns {
                                column-count: 3;
                            }
                        }

                        @media (min-width: 1500px) {
                            .card-columns {
                                column-count: 4;
                            }
                        }
                    </style>
                    <div class="card-columns">
                        @php $fileCount = 0; @endphp
                        @foreach($feedback->files as $file)
                        @php $fileCount++; @endphp
                        <div class="card">
                            <a href="{{ asset("storage/feedback/".$file) }}" target="_blank" style="margin: 0px auto">
                                <img src="{{ asset("storage/feedback/".$file) }}" alt="{{ $file }}"
                                    class="rounded card-img-top">
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @else
                    Null
                    @endif
                    @endif
                </div>
            </div>
        </form>

        @if($mode['isModeShow'])
        <div class="mt-4 mb-4 bg-white" style="padding: 16px">
            <h1>Message Board</h1>
            <hr>
            @if(sizeof($feedback->chat) > 0)
            @foreach($feedback->chat as $chat)
            @if($chat['user']=='admin')
            <div class="toast show" style="min-width: 350px; max-width: 500px;">
                <div class="toast-header bg-primary text-light">
                    <strong class="mr-auto"><i class="ti-headphone-alt"></i> {{$chat['user']}}</strong>
                    <small
                        class="ext-light">{{\Carbon\Carbon::parse(date('Y-m-d H:i:s',$chat['created_at']))->diffForHumans()}}</small>
                </div>
                <div class="toast-body">
                    {{$chat['body']}}
                </div>
            </div>
            @else
            <div class="toast show ml-auto" style="min-width: 350px; max-width: 500px;">
                <div class="toast-header">
                    <strong class="mr-auto"><img class="bdrs-50p" src="{{ auth()->user()->avatar }}" alt=""
                            style="width: 1.5rem"> {{$chat['user']}}</strong>
                    <small
                        class="ext-light">{{\Carbon\Carbon::parse(date('Y-m-d H:i:s',$chat['created_at']))->diffForHumans()}}</small>
                </div>
                <div class="toast-body">
                    {{$chat['body']}}
                </div>
            </div>
            @endif
            @endforeach
            @else
            <div class="card-footer">Not Message</div>
            @endif
            <hr>
            @if($feedback->status==config('constants.FEEDBACK_STATUS.OPEN'))
            <form method="post" action="{{URL::action('Panel\FeedbackController@update_chat',$feedback->id)}}">
                {{ csrf_field() }}
                {{method_field('put')}}
                <div class="form-group mt-1">
                    <label class="h3" for="message">Send Message</label>
                    <textarea class="form-control" name="message" rows="5" placeholder="Enter any content..."
                        required></textarea>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary btn-lg">Send Message</button>
                </div>
            </form>
            @endif
        </div>
        @endif

    </div>
    @endsection