@extends('admin.default')

@section('page-header')
Notification <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

@can('notification-create')
<div class="mB-20">
  <a href="{{ URL::action('Panel\NotificationController@create') }}" class="btn btn-info">
    {{ trans('app.add_button') }}
  </a>
  <a href="{{ URL::action('Panel\NotificationController@devices') }}" class="btn btn-info pull-right">
    Devices List
  </a>
</div>
@endcan

<div class="row">
  <div class="col-md-12">
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
      <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">

        <thead>
          <tr>
            <th>Title</th>
            <th>Sub heading</th>
            <th>Type</th>
            <th style='width:11%'>Action</th>
          </tr>
        </thead>

        <tfoot>
          <tr>
            <th>Title</th>
            <th>Sub heading</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </tfoot>

        <tbody>

          @foreach ($notifications as $notification)
          <tr>
            <td>
              <a href="{{ URL::action('Panel\NotificationController@show', $notification['id']) }}">
                {{ $notification->title }}
              </a>
            </td>

            <td>{{ $notification->sub_heading }}</td>

            <td>{{ config('variables.notification_type')[$notification->type] }}</td>

            <td>
              <ul class="list-inline">
                @if(auth()->user()->can('notification-edit'))
                <li class="list-inline-item">
                  <a href="{{ URL::action('Panel\NotificationController@edit', $notification['id']) }}"
                    title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">
                    <span class="ti-pencil"></span>
                  </a>
                </li>
                @endif
                @if(auth()->user()->can('notification-delete'))
                <li class="list-inline-item">
                  {!! Form::open([
                  'class'=>'delete',
                  'url' => URL::action('Panel\NotificationController@destroy', $notification['id']),
                  'method' => 'DELETE', ])
                  !!}
                  <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}">
                    <i class="ti-trash"></i>
                  </button>
                  {!! Form::close() !!}
                </li>
                @endif
              </ul>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection