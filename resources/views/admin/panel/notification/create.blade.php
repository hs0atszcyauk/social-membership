@extends('admin.default')

@section('page-header')
Notification <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
{!! Form::open([
'action' => ['Panel\NotificationController@store'],
'files' => true,
])
!!}

@include('admin.panel.notification.partial.form')

<div class="row mB-10">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd" id="target-group">
      {!! Form::mySelect('targets', 'Targets'.' <span style="color:red">*</span>',
      $roles_array, null
      ,array('required' => 'required'))!!}
    </div>

    <div class="bgc-white p-20 bd" id="one-target">
      <label for="target-email">Specific Targets<span style="color:red">*</span></label>
      <input type="text" name="target" id="target-email" class="form-control" placeholder="Input user email"
        list="email">
      <datalist id="email">
        @foreach ($emails as $email)
        <option value="{{$email->email}}" label="{{$email->email}}">
          @endforeach
      </datalist>
    </div>

  </div>
</div>


<div class="text-center">
  <button type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
  <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
</div>

{!! Form::close() !!}

@stop


@section('js')
<script>
  $(document).ready(function() {
    $('#type').val(1);
    $('#target-email').attr('name',null);
    $('#one-target').hide();
    
    $('#type').change(function(){
      if($(this).children("option:selected").val() == 1 || $(this).children("option:selected").val() == 1){
        $('#target-email').attr('name',null);
        $('#one-target').hide();
        $('#target-group').show();
      }
      
      if($(this).children("option:selected").val() == 3){
        $('#target-email').attr('name','target');
        $('#target-group').hide();
        $('#one-target').show();
      }
    });
  });
</script>
@endsection