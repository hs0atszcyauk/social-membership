@extends('admin.default')

@section('page-header')
User Reward <small>{{ $user->name['first'].' '.$user->name['last'].' - '.trans('app.detail_item') }}</small>
@stop

@section('content')

@include('admin.panel.reward.partial.panel_detail')

<div class="row mB-40">
  <div class="col-sm-10 m-auto">
    <div class="bgc-white p-20 bd">
      <div class="table-responsive">
        <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <table id="dataTable-reward" style="width: 100%;" class="table dataTable no-footer" role="grid"
            aria-describedby="datatable1_info">
            <thead>
              <tr role="row">
                <th>Date</th>
                <th>Content</th>
                <th>Point</th>
                @if(auth()->user()->can('reward-edit') || auth()->user()->can('reward-delete'))
                <th>Action</th>
                @endif
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Date</th>
                <th>Content</th>
                <th>Point</th>
                @if(auth()->user()->can('reward-edit') || auth()->user()->can('reward-delete'))
                <th>Action</th>
                @endif
              </tr>
            </tfoot>
            <tbody>
              @foreach($points as $point)
              <tr>
                <td>{{$point->created_at}}</td>
                <td>{{$point->content}}</td>
                <td class='text-{{($point->type==0)?'danger':'success'}}'>
                  {{($point->type==0)?'- ':'+ '}}{{$point->points}}</td>
                @if(auth()->user()->can('reward-edit') || auth()->user()->can('reward-delete'))
                <td>
                  <ul class="list-inline">
                    @can('reward-edit')
                    <li class="list-inline-item">
                      <a href="{{ URL::action('Panel\RewardController@edit', [$user->id,$point->id] ) }}"
                        title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span
                          class="ti-pencil"></span></a>
                    </li>
                    @endif
                    @can('reward-delete')
                    <li class="list-inline-item">
                      {!! Form::open([
                      'class'=>'delete',
                      'url' => URL::action('Panel\RewardController@destroy', [$user->id,$point->id]),
                      'method' => 'DELETE',
                      ])
                      !!}

                      <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i
                          class="ti-trash"></i></button>

                      {!! Form::close() !!}
                    </li>
                    @endcan
                  </ul>
                </td>
                @endif
              </tr>

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection