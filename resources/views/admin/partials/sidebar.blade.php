<div class="sidebar">
  <div class="sidebar-inner">
    <!-- ### $Sidebar Header ### -->
    <div class="sidebar-logo">
      <div class="peers ai-c fxw-nw">
        <div class="peer peer-greed">
          <a class='sidebar-link td-n' href="{{URL::action('Panel\UserController@index')}}">
            <div class="peers ai-c fxw-nw">
              <div class="peer">
                <div class="logo">
                  <img class="w-3r bdrs-50p mt-2 ml-1" src="{{asset('/images/icon.png')}}" alt="Socialize Logo">
                </div>
              </div>
              <div class="peer peer-greed">
                <h5 class="lh-1 mB-0 logo-text">Social Membership</h5>
              </div>
            </div>
          </a>
        </div>
        <div class="peer">
          <div class="mobile-toggle sidebar-toggle">
            <a href="" class="td-n">
              <i class="ti-arrow-circle-left"></i>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- ### $Sidebar Menu ### -->
    <ul class="sidebar-menu scrollable pos-r">
      @include('admin.partials.menu')
    </ul>
  </div>
</div>