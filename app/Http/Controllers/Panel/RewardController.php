<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\UserPointRecord;
use Illuminate\Http\Request;
use App\Models\User;

class RewardController extends Controller
{

    private $mode = [
        'isModeShow' => false,
        'isModeCreate' => false,
        'isModeDuplicate' => false,
        'isModeEdit' => false,
    ];

    function __construct()
    {
        $this->middleware('permission:reward-list', ['only' => ['index']]);
        $this->middleware('permission:reward-list|user-profile', ['only' => ['show']]);
        $this->middleware('permission:reward-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:reward-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:reward-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest('updated_at')->get();
        $points = UserPointRecord::latest('created_at')->get();
        return view('admin.panel.reward.index', compact('points', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add($id)
    {
        $user = User::find($id);
        $this->mode['isModeCreate'] = true;
        return view('admin.panel.reward.create', compact('user'))->with('mode', $this->mode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, UserPointRecord::rules());
        $record = new UserPointRecord();
        $record->user_id = $request->id;
        $record->type = $request->type;
        $record->points = $request->point;
        $record->content = $request->content;
        $record->save();
        $user = User::find($request->id);
        $point = $user->point;
        if ($request->type == 0) {
            $point -= $request->point;
        } else {
            $point += $request->point;
        }
        $user->point = $point;
        $user->save();

        $action = ($record->type == 1) ? 'add' : 'minus';
        PushNotification::send(
            User::find($request->id),
            "$request->point has been $action to your account",
            'Please check the detail in app',
            'PointRecord',
            array()
        );
        session()->flash('success', trans('app.success_store'));
        return redirect()->action('Panel\RewardController@show', $request->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->mode['isModeShow'] = true;
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }
        if (!auth()->user()->hasPermissionTo('reward-list')) {
            if ($id != auth()->user()->id) {
                abort(404);
            }
        }
        $points = UserPointRecord::where('user_id', $id)->orderBy('created_at', 'desc')->get();
        return view('admin.panel.reward.detail', compact('user', 'points'))->with('mode', $this->mode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $pointId)
    {
        $this->mode['isModeEdit'] = true;
        $point = UserPointRecord::find($pointId);
        $user = User::find($id);
        if (!$point) {
            abort(404);
        }
        return view('admin.panel.reward.edit', compact('user', 'point'))->with('mode', $this->mode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $pointId)
    {
        $this->validate($request, UserPointRecord::rules());
        $user = User::find($id);
        $record = UserPointRecord::find($pointId);
        $point = $user->point;
        if ($record->type == 0) {
            $point += $record->points;
        } else {
            $point -= $record->points;
        }
        $record->user_id = $request->id;
        $record->type = $request->type;
        $record->points = $request->point;
        $record->content = $request->content;
        $record->save();
        if ($request->type == 0) {
            $point -= $request->point;
        } else {
            $point += $request->point;
        }
        $user->point = $point;
        $user->save();
        session()->flash('success', trans('app.success_store'));
        return redirect()->action('Panel\RewardController@show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $pointId)
    {
        $user = User::find($id);
        $record = UserPointRecord::find($pointId);
        $point = $user->point;
        if ($record->type == 0) {
            $point += $record->points;
        } else {
            $point -= $record->points;
        }
        $user->point = $point;
        $user->save();
        UserPointRecord::destroy($pointId);
        session()->flash('success', trans('app.success_destroy'));
        return redirect()->action('Panel\RewardController@show', $record->user_id);
    }
}
