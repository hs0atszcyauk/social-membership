<?php

namespace App\Http\Controllers\Panel;

use App\Models\Settings\FeedbackType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Http\PushNotification;
use App\Models\User;

class FeedbackController extends Controller
{

    private $mode = [
        'isModeShow' => false,
        'isModeCreate' => false,
        'isModeEdit' => false,
        'isUserList' => false,
    ];

    function __construct()
    {
        //        $this->middleware('permission:feedback-list',['only' => ['index','show']]);
        //        $this->middleware('permission:feedback-create',['only' => ['create','store']]);
        //        $this->middleware('permission:feedback-edit',['only' => ['edit','update']]);
        //        $this->middleware('permission:feedback-delete',['only' => ['destroy']]);
        //        $this->middleware('permission:user-profile',['only' => ['show','edit','update']]);
    }

    public function index()
    {
        $feedbacks = Feedback::all();
        $feedbacks->load('user');
        $feedbacks->load('feedbackType');
        return view('admin.panel.feedback.index', compact('feedbacks'))->with('mode', $this->mode);
    }

    public function user_list($user)
    {
        $this->mode['isUserList'] = true;
        $feedbacks = Feedback::where('user_id', auth()->user()->id)->get();
        $feedbacks->load('feedbackType');
        return view('admin.panel.feedback.index', compact('feedbacks'))->with('mode', $this->mode);
    }

    public function show($id)
    {
        $this->mode['isModeShow'] = true;

        $feedback = Feedback::find($id);
        $types = FeedbackType::all();
        return view('admin.panel.feedback.detail', compact('feedback', 'types'))->with('mode', $this->mode);
    }

    public function create()
    {
        $this->mode['isModeCreate'] = true;
        $types = FeedbackType::where('status', '1')->get();
        return view('admin.panel.feedback.detail', compact('types'))->with('mode', $this->mode);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'content' => 'required',
            'type' => 'required',
            [
                'file_upload.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'
            ], [
                'file_upload.*.mimes' => 'Only jpeg,png and bmp images are allowed',
                'file_upload.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
            ]
        ]);

        $feedback = new Feedback();
        $files_array = array();
        if (!is_null($request->file_upload)) {
            foreach ($request->file_upload as $upload_item) {
                $file = $upload_item->store('feedback');
                array_push($files_array, basename($file));
            }
            $feedback->files = serialize($files_array);
        } else {
            $feedback->files = serialize(array());
        }

        $message = [];
        $adminMessage = [
            'user' => 'admin',
            'body' => 'We have received your information. We will reply soon.',
            'created_at' => time(),
        ];
        array_push($message, $adminMessage);

        $feedback->user_id = auth()->user()->id;
        $feedback->feedback_type_id = $request->type;
        $feedback->content = serialize($request->content);
        $feedback->chat = serialize($message);
        $feedback->save();

        session()->flash('message', "Feedback created.");
        return redirect()->action('Panel\FeedbackController@show', $feedback->id);
    }

    public function edit($id)
    {
        $this->mode['isModeEdit'] = true;

        $feedback = Feedback::find($id);
        $types = FeedbackType::all();
        return view('admin.panel.feedback.detail', compact('feedback', 'types'))->with('mode', $this->mode);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'status' => 'required',
            'priority' => 'required',
            'feedback_id' => 'required',
        ]);

        $feedback = Feedback::find($request->feedback_id);
        $feedback->status = $request->status;
        $feedback->priority = $request->priority;
        $feedback->save();

        return redirect()->action('Panel\FeedbackController@show', $feedback->id);
    }

    public function update_chat(Request $request, $feedback_id)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);

        $feedback = Feedback::find($feedback_id);
        $chat = $feedback->chat;
        $message = [
            'user' => (auth()->user()->hasRole('Admin')) ? "admin" : auth()->user()->email,
            'body' => $request->message,
            'created_at' => time(),
        ];
        array_push($chat, $message);
        $feedback->chat = serialize($chat);
        $feedback->save();

        if (auth()->user()->hasRole('Admin')) {
            PushNotification::send(
                User::find($feedback->user_id),
                'Administrator has response your feedback, please check it in the app',
                $request->message,
                'NotificationList',
                array()
            );
        }
        return redirect()->action('Panel\FeedbackController@show', $feedback->id);
    }
}
