<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Receipt;
use App\Http\PushNotification;
use App\Models\User;

class ReceiptController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:receipt-list', ['only' => ['index', 'show']]);
        $this->middleware('permission:receipt-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:receipt-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:receipt-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = User::all();
        $approving = Receipt::where('status', '0')->orderBy('created_at', 'desc')->get();
        $approved = Receipt::where('status', '3')->orderBy('created_at', 'desc')->get();
        $disapprove = Receipt::where('status', '2')->orderBy('created_at', 'desc')->get();
        return view('admin.panel.receipt.index', compact('approving', 'approved', 'disapprove'))
            ->withModel($model);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receipts = Receipt::find($id);
        $receipts->expense_type = config('variables.expense_type.' . $receipts->expense_type);
        $receipts->payment_type = config('variables.payment_type.' . $receipts->payment_type);
        $receipts->location = config('variables.location.' . $receipts->location);
        $receipts->currency_id = config('variables.currency.' . $receipts->currency_id);
        return view('admin.panel.receipt.detail', compact('receipts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Receipt::find($id)->update([
            'comment' => $request->comment
        ]);
        session()->flash('success', trans('app.success_update'));
        return redirect()->action('Panel\ReceiptController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Receipt::destroy($id);
        session()->flash('success', trans('app.success_destroy'));
        return redirect()->action('Panel\ReceiptController@index');
    }

    public function approve(Request $request, $id)
    {
        Receipt::find($id)->update([
            'status' => $request->status,
            'comment' => $request->comment
        ]);

        if ($request->status == 3) {
            PushNotification::send(
                User::find(Receipt::find($id)->user_id),
                'Your expense is approved',
                'You can claim the budgets',
                'ShowExpense',
                array('id' => $id)
            );
            session()->flash('success', 'Approve Succeed');
        } else if ($request->status == 2) {
            PushNotification::send(
                User::find(Receipt::find($id)->user_id),
                'Your expense is not approved',
                'Please see the comment',
                'ShowExpense',
                array('id' => $id)
            );
            session()->flash('success', 'Approve Fail');
        }

        return redirect()->action('Panel\ReceiptController@index');
    }
}
