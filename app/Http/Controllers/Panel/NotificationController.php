<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification\Notification;
use App\Models\Notification\NotificationDevice;
use App\Models\Notification\NotificationUser;
use Spatie\Permission\Models\Role;
use App\Http\PushNotification;
use App\Models\User;

class NotificationController extends Controller
{

    private $mode = [
        'isModeShow' => false,
        'isModeCreate' => false,
        'isModeDuplicate' => false,
        'isModeEdit' => false,
    ];

    function __construct()
    {
        $this->middleware('permission:notification-list', ['only' => ['index', 'show']]);
        $this->middleware('permission:notification-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:notification-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:notification-delete', ['only' => ['destroy']]);
        //        $this->middleware('permission:user-profile',['only' => ['show','edit','update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::all();
        return view('admin.panel.notification.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emails = User::select('email')->get();
        $roles_array[0] = "All";
        $roles = Role::select('id', 'name')->get();
        foreach ($roles as $role) {
            $roles_array[$role->id] = $role->name;
        }

        $this->mode['isModeCreate'] = true;
        $notifications = Notification::all();
        return view('admin.panel.notification.create',  compact('notifications', 'roles_array', 'emails'))->with('mode', $this->mode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $target = null;
        if (isset($request->target)) {
            if (User::where('email', $request->target)->first() === null) {
                session()->flash('fail', 'Error: Invalid User Email.');
                return redirect()->back();
            }
            $target = User::where('email', $request->target)->first();
        } else {
            $target = $request->targets;
        }

        $notifications = new Notification;
        if (isset($request->image)) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('image')->storeAs(config('variables.folder.notifications'), $fileNameToStore);
            $notifications->image = $fileNameToStore;
        }


        $notifications->title = $request->title;
        $notifications->content = $request->content;
        $notifications->time = $request->time;
        $notifications->date = $request->date;
        $notifications->type = $request->type;
        $notifications->save();
        session()->flash('success', trans('app.success_store'));



        PushNotification::send(
            $target,
            $request->title,
            $request->sub_heading,
            'Notification',
            array('id' => $notifications->id, 'read' => '0', 'read_id' => $notifications->id),
            $notifications->id
        );

        return redirect()->action('Panel\NotificationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->mode['isModeShow'] = true;
        $notifications = Notification::find($id);
        return view('admin.panel.notification.detail', compact('notifications'))->with('mode', $this->mode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->mode['isModeEdit'] = true;
        $notifications = Notification::find($id);
        return view('admin.panel.notification.edit', compact('notifications'))->with('mode', $this->mode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notifications = Notification::find($id);

        if (isset($request->image)) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('image')->storeAs(config('variables.folder.notifications'), $fileNameToStore);
            $notifications->image = $fileNameToStore;
        }

        $notifications->title = $request->title;
        $notifications->sub_heading = $request->sub_heading;
        $notifications->content = $request->content;
        $notifications->attachment = $request->attachment;
        $notifications->location = $request->location;
        $notifications->date = $request->date;
        $notifications->type = $request->type;
        $notifications->save();
        session()->flash('success', trans('app.success_update'));
        return redirect()->action('Panel\NotificationController@show', $notifications->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Notification::destroy($id);
        NotificationUser::where('notification_id', $id)->delete();
        session()->flash('success', trans('app.success_destroy'));
        return redirect()->action('Panel\NotificationController@index');
    }

    public function devices()
    {
        $devices = NotificationDevice::with('user')->get();
        return view('admin.panel.notification.devices', compact('devices'));
    }
}
