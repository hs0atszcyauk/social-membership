<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event\Event;
use App\Models\User;
use App\Models\Event\EventUser;
use App\Models\Settings\EventType;
use Spatie\Permission\Models\Role;
// use Illuminate\Database\Eloquent\Builder;

class EventController extends Controller
{

    private $mode = [
        'isModeShow' => false,
        'isModeCreate' => false,
        'isModeDuplicate' => false,
        'isModeEdit' => false,
    ];

    function __construct()
    {
        $this->middleware('permission:event-list', ['only' => ['index', 'show']]);
        $this->middleware('permission:event-add', ['only' => ['add']]);
        $this->middleware('permission:event-withdraw', ['only' => ['withdraw']]);
        $this->middleware('permission:event-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:event-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:event-delete', ['only' => ['destroy']]);
        //        $this->middleware('permission:user-profile',['only' => ['show','edit','update']]);
    }

    public function index()
    {
        $events = Event::all();
        return view('admin.panel.event.index', compact('events'));
    }

    public function create()
    {
        $this->mode['isModeCreate'] = true;
        $eventTypes = EventType::all();
        return view('admin.panel.event.create',  compact('eventTypes'))->with('mode', $this->mode);
    }

    public function store(Request $request)
    {
        $fileNameToStore = "";
        if (isset($request->image)) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('image')->storeAs(config('variables.folder.events'), $fileNameToStore);
        }

        $events = new Event;
        $events->title              = $request->title;
        $events->content            = $request->content;
        $events->type               = $request->type;
        $events->reward             = $request->reward;
        $events->limit_of_apply     = $request->limit_of_apply;
        $events->image              = $fileNameToStore;

        $events->valid_from         = $request->valid_from;
        $events->valid_to           = $request->valid_to;
        $events->venue              = $request->venue;
        $events->time               = "$request->date $request->time_from - $request->time_to";

        $events->save();
        session()->flash('success', trans('app.success_store'));
        return redirect()->action('Panel\EventController@index', $events->id);
    }

    public function show($id)
    {
        $this->mode['isModeShow'] = true;
        $events = Event::find($id);
        $eventTypes = EventType::all();
        return view('admin.panel.event.detail', compact('events', 'eventTypes'))->with('mode', $this->mode);
    }

    public function edit($id)
    {
        $this->mode['isModeEdit'] = true;
        $events = Event::find($id);
        $eventTypes = EventType::all();

        $time = explode(" ", $events->time);
        $events->date = $time[0];
        $events->time_from = $time[1];
        $events->time_to = $time[3];
        return view('admin.panel.event.edit', compact('events', 'eventTypes'))->with('mode', $this->mode);
    }

    public function update(Request $request, $id)
    {
        $events = Event::find($id);
        if (isset($request->image)) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = str_random(16) . '_' . time() . '.' . $extension;
            $request->file('image')->storeAs(config('variables.folder.events'), $fileNameToStore);

            $events->image          = $fileNameToStore;
        }

        $events->title              = $request->title;
        $events->content            = $request->content;
        $events->type               = $request->type;
        $events->reward             = $request->reward;
        $events->limit_of_apply     = $request->limit_of_apply;
        $events->valid_from         = $request->valid_from;
        $events->valid_to           = $request->valid_to;
        $events->venue              = $request->venue;
        $events->time               = "$request->date $request->time_from - $request->time_to";

        $events->save();
        session()->flash('success', trans('app.success_update'));
        return redirect()->action('Panel\EventController@index', $events->id);
    }

    public function destroy($id)
    {
        Event::destroy($id);
        session()->flash('success', trans('app.success_destroy'));
        return redirect()->action('Panel\EventController@index');
    }
}
