<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Notification\Notification;
use App\Models\Notification\NotificationUser;
use Illuminate\Support\Facades\Auth;

class NotificationController extends ApiController
{
    public function index()
    {

        try {
            $query = Notification::where('type', '!=', '3')->orderBy('created_at', 'desc')->get();
            return parent::sendResponse('data', $query, 'Notification Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function show($id)
    {
        try {
            $query = Notification::where('id', $id)->first();
            return parent::sendResponse('data', $query, 'Notification Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function personal()
    {
        $query = NotificationUser::where('user_id', Auth::guard('api')->user()->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return parent::sendResponse('data', $query, 'Notification Data');
    }

    public function personal_delete($id)
    {

        try {
            if (NotificationUser::find($id)->delete()) {
                return parent::sendResponse('message', 'success', 'Notification Data');
            } else {
                return parent::sendResponse('message', 'fail', 'Notification Data');
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function read($id)
    {
        try {
            if (NotificationUser::find($id)->update(['read' => true])) {
                return parent::sendResponse('message', 'success', 'Notification Data');
            } else {
                return parent::sendResponse('message', 'fail', 'Notification Data');
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }
}
