<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserPointRecord;
use App\Models\Receipt;
use Carbon\Carbon;
use App\Http\PushNotification;

class AdminController extends ApiController
{
    public function user_list()
    {
        $query = User::select('id', 'email', 'name', 'status', 'avatar')->orderBy('status', 'asc')->get();
        return parent::sendResponse('data', $query, 'All User Data');
    }

    public function user_show($id)
    {
        $query = User::findOrFail($id);
        $query = User::where('id', $id)->first();
        $query->role = $query->roles->pluck('name');
        return parent::sendResponse('data', $query, 'User Data');
    }

    public function user_approve($id)
    {
        if (User::findOrFail($id)->update(['status' => 1, 'added_at' => Carbon::now()])) {
            PushNotification::send(
                User::find($id),
                'Your account has been approved',
                'Please check the detail in app',
                'Home',
                array()
            );
            UserPointRecord::create([
                'user_id' => $id,
                'content' => 'New User Rewards Package',
                'points' => 100,
                'type' => 1
            ]);

            User::find($id)->assignRole('Member');

            return parent::sendResponse('status', "success", 'Approve Success');
        } else {
            return parent::sendError('Cannot Approve', 215);
        }
    }

    public function user_ban($id)
    {
        if (User::findOrFail($id)->update(['status' => 2])) {
            PushNotification::send(
                User::find($id),
                'Your account has been banned',
                'Please check the detail in app',
                'Home',
                array()
            );

            return parent::sendResponse('status', "success", 'Ban Success');
        } else {
            return parent::sendError('Cannot Ban', 215);
        }
    }

    public function user_unban($id)
    {
        if (User::findOrFail($id)->update(['status' => 1])) {
            PushNotification::send(
                User::find($id),
                'Your account unlocked by adminstrator',
                'Please check the detail in app',
                'Home',
                array()
            );

            return parent::sendResponse('status', "success", 'Unban Success');
        } else {
            return parent::sendError('Cannot Unban', 215);
        }
    }

    public function receipt_list()
    {
        try {
            $userTB = config('variables.tables_name')['003'];
            $receiptTB = config('variables.tables_name')['010'];

            $query = Receipt::select('id', 'title', 'location', 'amount')
                ->join($userTB, "$receiptTB.user_id", "=", "$userTB.id")
                ->select("$userTB.email", "$receiptTB.*")
                ->orderBy('created_at', 'desc')
                ->take(30)
                ->get();

            return parent::sendResponse('data', $query, 'Receipt Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function receipt_show($id)
    {
        try {
            $query = Receipt::findOrFail($id);
            return parent::sendResponse('data', $query, 'Receipt Data');
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }

    public function receipt_comment(Request $request, $id)
    {
        try {
            if (Receipt::findOrFail($id)->update([
                'comment' => $request->comment,
                'status' => $request->status
            ])) {

                $title = ($request->status == 1) ? 'Your expense is approved' : 'Your expense is not approved';
                $sub_title = ($request->status == 1) ? 'You can claim the budgets' : 'Please see the comment';

                PushNotification::send(
                    User::find(Receipt::find($id)->user_id),
                    $title,
                    $sub_title,
                    'ShowExpense',
                    array('id' => $id)
                );

                return parent::sendResponse('status', "success", 'Update Success');
            } else {
                return parent::sendError('Cannot update', 215);
            }
        } catch (\Exception $e) {
            return parent::sendError('Unexpected error occurs, please contact admin and see what happen.', 216);
        }
    }
}
