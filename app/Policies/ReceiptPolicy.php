<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Receipt;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReceiptPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the receipt.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Receipt  $receipt
     * @return mixed
     */
    public function view(User $user, Receipt $receipt)
    {
        //
    }

    /**
     * Determine whether the user can create receipts.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the receipt.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Receipt  $receipt
     * @return mixed
     */
    public function update(User $user, Receipt $receipt)
    {
        return $user->id === $receipt->user_id;
    }

    /**
     * Determine whether the user can delete the receipt.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Receipt  $receipt
     * @return mixed
     */
    public function delete(User $user, Receipt $receipt)
    {
        //
    }
}
