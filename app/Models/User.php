<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Notification\NotificationDevice;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $keyType = 'string';
    protected $guarded = [];  //protected $fillable

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'password', 'remember_token',
    ];


    public function getTable()
    {
        return config('variables.tables_name')['003'];
    }

    public function device()
    {
        return $this->hasOne(NotificationDevice::Class);
    }
    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null)
    {
        $commun = [
            'email'    => "required|email",
            'password' => 'nullable|confirmed',
            'avatar'   => 'image',
            'birth'    => 'date',
        ];

        if ($update) {
            return $commun;
        }

        return array_merge($commun, [
            'email'    => 'required|email|max:191',
            'password' => 'required|confirmed|min:6',
            'birth'    => 'date',
        ]);
    }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function setPasswordAttribute($value = '')
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getAvatarAttribute($value)
    {
        if (!$value) {
            return asset('images/avatar.png');
        }

        return asset(config('variables.avatar.public') . $value);
    }

    public function getNameAttribute($value)
    {
        return unserialize($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    /*
    |------------------------------------------------------------------------------------
    | Boot
    |------------------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            $user->attributes['id'] = 'USER-' . str_random(16);
        });

        static::updating(function ($user) {
            $original = $user->getOriginal();

            if (\Hash::check('', $user->password)) {
                $user->attributes['password'] = $original['password'];
            }
        });
    }

    /**
     *
     *
     */

    public function Feedback()
    {
        return $this->hasMany(Feedback::Class);
    }
}
