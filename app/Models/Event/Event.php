<?php

namespace App\Models\Event;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    protected $guarded = [];  // protected $fillable
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['date'];

    public function getTable()
    {
        return config('variables.tables_name')['008'];
    }

    public function getImageAttribute($value)
    {
        return config('variables.image.events') . $value;
    }
    public function getDateAttribute()
    {
        return explode(' ', $this->time)[0];
    }
}
