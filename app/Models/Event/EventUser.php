<?php

namespace App\Models\Event;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class EventUser extends Model
{

    protected $primaryKey = ['event_id', 'user_id'];
    protected $guarded = [];
    protected $hidden = ['updated_at'];

    public $incrementing = false;

    public function getTable()
    {
        return config('variables.tables_name')['009'];
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d F, Y', strtotime($value));
    }
}
