<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPointRecord extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['012'];
    }

    public static function rules($update = false, $id = null)
    {
        $commun = [
            'point'     => "required",
            'content'   => 'required',
        ];

        if ($update) {
            return $commun;
        }

        return array_merge($commun, [
            'point'     => "required",
            'content'   => 'required',
        ]);
    }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function getCreatedAtAttribute($value)
    {
        return date('d F, Y', strtotime($value));
    }
}
