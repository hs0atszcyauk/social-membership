<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['001'];
    }
}
