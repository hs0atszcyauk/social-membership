<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['xxx'];
    }
}
