<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['updated_at'];

    public function getTable()
    {
        return config('variables.tables_name')['006'];
    }

    public function getImageAttribute($value)
    {

        return config('variables.image.notifications') . $value;
    }
}
