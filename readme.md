# Social Membership
**_[Laravel](https://laravel.com/) PHP Framework with [Laradminator](https://github.com/kossa/laradminator)_**  as admin dash


## Setup:
All you need is to run these commands:
```bash
git clone
cd social-membership
composer install
sudo chmod 777 storage/ -R
cp .env.example .env
php artisan key:generate
php artisan migrate:fresh --seed
php artisan storage:link
npm install
npm run production
```
