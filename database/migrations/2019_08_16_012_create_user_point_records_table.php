<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPointRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    function __construct()
    {
        $this->tableName = config('variables.tables_name')['012'];
        $this->users = config('variables.tables_name')['003'];
    }

    public function up()
    {
        Schema::dropIfExists($this->tableName);
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on($this->users)
                ->onDelete('cascade');

            $table->text('content');
            $table->integer('points');
            $table->boolean('type');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
