<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    function __construct()
    {
        $this->tableName = config('variables.tables_name')['010'];
        $this->users = config('variables.tables_name')['003'];
        $this->currencies = config('variables.tables_name')['001'];
    }

    public function up()
    {
        Schema::dropIfExists($this->tableName);
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on($this->users)
                ->onDelete('cascade');

            $table->date('transacted_at');
            $table->unsignedInteger('currency_id');
            $table->foreign('currency_id')
                ->references('id')
                ->on($this->currencies)
                ->onDelete('cascade');

            $table->integer('location');
            $table->integer('expense_type');
            $table->integer('payment_type');
            $table->double('amount');
            $table->text('image')->nullable();
            $table->integer('status')->nullable();
            $table->text('remarks')->nullable();
            $table->text('comment')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
