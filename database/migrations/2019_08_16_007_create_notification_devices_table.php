<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    function __construct()
    {
        $this->tableName = config('variables.tables_name')['007'];
        $this->users = config('variables.tables_name')['003'];
    }

    public function up()
    {

        Schema::dropIfExists($this->tableName);
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->string('device_id')->primary();

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on($this->users)
                ->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
