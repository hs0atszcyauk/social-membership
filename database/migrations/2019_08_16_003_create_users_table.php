<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    function __construct()
    {
        $this->tableName = config('variables.tables_name')['003'];
        $this->currencies = config('variables.tables_name')['001'];
    }

    public function up()
    {
        Schema::dropIfExists($this->tableName);
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('address')->nullable();
            $table->date('birth');
            $table->integer('status')->default(0);
            $table->integer('point')->default(0);
            $table->string('phone')->nullable();
            $table->string('avatar')->default('icon.png');
            $table->unsignedInteger('currency')->default(1);
            $table->foreign('currency')
                ->references('id')
                ->on($this->currencies)
                ->onDelete('cascade');

            $table->text('remarks')->nullable();
            $table->string('api_token', 64)->nullable(); //random generate in User Model
            // $table->unsignedInteger('setting_id');
            // $table->foreign('setting_id')
            //     ->references('id')
            //     ->on('user_settings')
            //     ->onDelete('cascade');

            $table->date('added_at')->nullable();;
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('variables.tables_name')['011']);
        Schema::dropIfExists($this->tableName);
    }
}
