<?php

use App\Models\Settings\FeedbackType;
use Illuminate\Database\Seeder;

class FeedbackTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FeedbackType::create(["name"=>"Feedback"]);
        FeedbackType::create(["name"=>"Internet service"]);
        FeedbackType::create(["name"=>"Communication service"]);
        FeedbackType::create(["name"=>"System Tool"]);
        FeedbackType::create(["name"=>"Security"]);
        FeedbackType::create(["name"=>"Data Storage"]);
        FeedbackType::create(["name"=>"Subscription & Payment"]);
        FeedbackType::create(["name"=>"Other"]);

    }
}
