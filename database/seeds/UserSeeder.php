<?php

use App\Models\User;
use App\Models\UserPointRecord;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        User::truncate();
        UserPointRecord::truncate();
        $data = [];
        $num_user = 5;

        //  Admin Account
        array_push($data, [
            'name' => serialize([
                'first' => 'admin',
                'last' => 'test',
            ]),
            'email' => 'a@test.com',
            'password' => Hash::make('123456'),
            'status' => 1,
            'point' => 100,
            'birth' => $faker->date(),
            'phone' => $faker->phoneNumber,
            'address'   => $faker->address,
            'api_token' => "Pr6cwRomXkXXEm1jioUYDl9e2DGxDt36PY19XMUrlwAPtNCBf36OxnR6mstvbRmf",
            'added_at' => now(),
        ]);

        //  Test User Account
        for ($i = 1; $i <= $num_user; $i++) {
            array_push($data, [
                'name' => serialize([
                    'first' => $i,
                    'last' => 'User',
                ]),
                'email' => $i.'@test.com',
                'password' => Hash::make('123456'),
                'status' => 1,
                'point' => 100,
                'birth' => $faker->date(),
                'phone' => $faker->phoneNumber,
                'address'   => $faker->address,
                'api_token' => str_random(64),
                'added_at' => now(),
            ]);
        }
        User::insert($data);


        UserPointRecord::create([
            'user_id'   => User::where('email','a@test.com')->first()->id,
            'content'   => 'New User Rewards Package',
            'points'    => 100,
            'type'      => true,
        ]);
        $user = User::where('email','a@test.com')->first();
        $user->assignRole('Admin');

        for ($i = 1; $i <= $num_user; $i++) {
            UserPointRecord::create([
                'user_id'   => User::where('email',$i.'@test.com')->first()->id,
                'content'   => 'New User Rewards Package',
                'points'    => 100,
                'type'      => true,
            ]);
            $user = User::where('email',$i.'@test.com')->first();
            $user->assignRole('User');
        }
    }
}
