<?php

use App\Models\Settings\Currency;
use Illuminate\Database\Seeder;

class SettingCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::truncate();
        Currency::create([
            'currency'  =>  'HKD',
            'rate' => 1,
        ]);
        Currency::create([
            'currency'  =>  'RMB',
            'rate' => 1.12,
        ]);
        Currency::create([
            'currency'  =>  'USD',
            'rate' => 7.8,
        ]);
        Currency::create([
            'currency'  =>  'EUR',
            'rate' => 8.7,
        ]);
        Currency::create([
            'currency'  =>  'GBP',
            'rate' => 9.5,
            'status' => 0,
        ]);
    }
}
