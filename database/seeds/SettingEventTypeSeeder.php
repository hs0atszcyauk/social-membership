<?php

use App\Models\Settings\EventType;
use Illuminate\Database\Seeder;

class SettingEventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventType::truncate();
        EventType::create([
            'type'  =>  'News',
        ]);
        EventType::create([
            'type'  =>  'Events',
        ]);

    }
}
