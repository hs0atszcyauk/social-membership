<?php

use App\Models\User;
use App\Models\Feedback;
use Illuminate\Database\Seeder;

class FeedbackSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();
    Feedback::truncate();
    $data = [];

    for ($i = 1; $i <= 5; $i++) {
      array_push($data, [
        'id'      => $i,
        'user_id' => $faker->numberBetween($min=1,$max=2),
        'title'   => $faker->sentence,
        'content' => $faker->text,
        'target'  => $faker->name,
        'type'    => $faker->randomDigitNotNull,
      ]);
    }
    Feedback::insert($data);
  }
}