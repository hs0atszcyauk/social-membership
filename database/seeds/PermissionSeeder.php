<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         *  Admin Permissions
         */
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',

            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'user-profile',

            'reward-list',
            'reward-create',
            'reward-edit',
            'reward-delete',

            'event-list',
            'event-create',
            'event-edit',
            'event-delete',

            'notification-list',
            'notification-create',
            'notification-edit',
            'notification-delete',

            'feedback-list',
            'feedback-create',
            'feedback-edit',
            'feedback-delete',

            'receipt-list',
            'receipt-create',
            'receipt-edit',
            'receipt-delete',

            'event-add',
            'event-withdraw',
        ];
        foreach ($permissions as $permission){
            Permission::create(['name' => $permission]);
        }

        $role = Role::create(['name' => 'Admin']);
        $permissions_Admin = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',

            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'user-profile',

            'reward-list',
            'reward-create',
            'reward-edit',
            'reward-delete',

            'event-list',
            'event-create',
            'event-edit',
            'event-delete',

            'notification-list',
            'notification-create',
            'notification-edit',
            'notification-delete',

            'feedback-list',
            'feedback-edit',

            'receipt-list',
            'receipt-edit',

        ];
        $role->givePermissionTo($permissions_Admin);

        /**S
         *  User Roles
         */
        $role = Role::create(['name' => 'User']);
        $permissions_User = [
            'user-profile',

            'role-list',

            'event-list',

            'notification-list',

            'feedback-list',
            'feedback-create',
            'feedback-edit',
            'feedback-delete',
            
            'receipt-list',
            'receipt-create',
            'receipt-edit',
            'receipt-delete',

            'event-add',
            'event-withdraw',
        ];

        $role->givePermissionTo($permissions_User);

        /**
         *  Other Roles
         */
        Role::create(['name' => 'Staff']);
        Role::create(['name' => 'Member']);
        Role::create(['name' => 'Junior Member']);
        Role::create(['name' => 'Senior Member']);
    }
}
