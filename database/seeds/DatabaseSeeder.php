<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // $this->call(PermissionSeeder::class);
        //$this->call(SettingCurrencySeeder::class);
        // $this->call(UserSeeder::class);
        //$this->call(EventsSeeder::class);
        //$this->call(SettingEventTypeSeeder::class);
        // $this->call(FeedbackTypeSeeder::class);
        //DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Eloquent::unguard();
        // only event table data
        DB::unprepared(file_get_contents('database/seeds/sql_files/social_membership.sql'));
    }
}
